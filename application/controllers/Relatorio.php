<?php

    class Relatorio extends MY_Controller{

        public function __construct(){
            
            parent::__construct();
            
            $this->load->model('relatorio_model');
            $this->load->model('curso_model');
      
        }

        public function formularioAlunoCurso() {

            $this->load->helper('form');
            $this->load->library('form_validation');

            $dados['subtitulo'] = "Relatório";
            $dados['titulo'] = "Aluno por Curso";

            $dados['cursos'] = $this->curso_model->get();

            $this->template->load('template', 'relatorio/formulario-aluno-curso', $dados);
        
        }
      
        public function alunoCurso() {

            $curso   = $this->input->post('curso_id');
            
            $dados['alunos']   = $this->relatorio_model->getAlunoByCurso($curso);
    
            $this->load->library('MY_FPDF');        
            $this->load->view('relatorio/alunoPorCursoPDF', $dados);
        
        }

        public function usuarioInativoPDF() {

            $dados['usuarios'] = $this->relatorio_model->getUsuariosInativos($curso);
            
            $this->load->library('MY_FPDF');
            
            $this->load->view('relatorio/usuarioInativoPDF', $dados);
        
        }

    }

 ?>