<?php

    class Login extends CI_Controller{

        public function __construct(){

            parent::__construct();

            $this->load->helper('form');
            $this->load->model('usuario_model');

        }

        public function index(){
            $this->load->view("login");
        }

        public function validar(){
   
            $dados = $this->input->post();
            $dados['senha'] = md5($dados['senha']);

            $registro = $this->usuario_model->buscarUsuarioAtivo($dados);
            if($registro != null){
            
                $this->session->set_userdata('logado', $registro);
                redirect('dashboard');
            
            }else{
            
                $dados['msg'] = "Login ou Senha incorreto ou Usuário inativo.";
                $this->load->view("login", $dados);

            }

        }

        public function logout(){

            $this->session->unset_userdata('logado', array());
            redirect('login');
      
        }
    
  }

 ?>