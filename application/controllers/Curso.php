<?php

    class Curso extends CI_Controller{

        public function __construct(){

            parent::__construct();

            $this->load->helper('form');
            $this->load->model('curso_model');

        }

        public function index(){

            $dados['subtitulo']= "Curso";
            $dados['titulo']= "Lista de Curso";
            $dados['cursos'] = $this->curso_model->get();

            $this->template->load('template', 'curso/lista-curso', $dados);
            
        }

        public function remover($id){
  
            if(!$this->curso_model->remover($id)){
                die('Erro ao tentar remover');
            }
  
            redirect('area/index');
  
        }
  
        public function cadastrar($id=null){

            $this->load->helper('form');
            $this->load->library('form_validation');
            
            $dados['subtitulo'] = "Curso";
            $dados['titulo'] = "Cadastro do Curso";
  
            $this->form_validation->set_rules('nome', 'Nome', 'required');
    
            $dados['acao'] = "curso/cadastrar/";
  
            $dados['registro'] = null; 
            if($id!==null){

                $dados['acao'] .= $id; 
                $dados['registro'] = $this->curso_model->get($id);
            
            }

            $this->load->model('area_model');
            $dados['listaArea'] = $this->area_model->get();
            $dados['cursos'] = $this->curso_model->get();
  
            if($this->form_validation->run() === false){
                $this->template->load('template', 'curso/formulario-curso', $dados);
            }else{
  
                if(!$this->curso_model->cadastrar($id)){
                    die("Erro ao tentar cadastrar os dados");
                }

                redirect('curso/index');
            
            }
  
        }

    }

 ?>