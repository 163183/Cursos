<?php 

    class Aluno extends CI_Controller{

        public function __construct(){

            parent::__construct();

            $this->load->helper('form');
            
            $this->load->model('aluno_model');
            $this->load->model('curso_model');

        }

        public function index(){

            $dados['subtitulo']= "Aluno";
            $dados['titulo']= "Lista de Alunos";
            $dados['alunos'] = $this->aluno_model->get();

            $this->template->load('template', 'aluno/lista-aluno', $dados);
            
        }

        public function remover($id){
  
            if(!$this->aluno_model->remover($id)){
                die('Erro ao tentar remover');
            }
  
            redirect('aluno/index');
  
        }

        public function cadastrar($id=null){

            $this->load->helper('form');
            $this->load->library('form_validation');
            
            $dados['subtitulo'] = "Aluno";
            $dados['titulo'] = "Cadastro da Aluno";
  
            $this->form_validation->set_rules('nome', 'Nome', 'required');
            $this->form_validation->set_rules('cpf', 'CPF', 'required');
            $this->form_validation->set_rules('rg', 'RG', 'required');

            $dados['acao'] = "aluno/cadastrar/";
  
            $dados['registro'] = null; 
            if($id!==null){

                $dados['acao'] .= $id; 
                $dados['registro'] = $this->aluno_model->get($id);
            
            }

            $dados['cursos'] = $this->curso_model->get();
  
            if($this->form_validation->run()===false){
                $this->template->load('template', 'aluno/formulario-aluno', $dados);
            }else{
  
                if(!$this->aluno_model->cadastrar($id)){
                    die("Erro ao tentar cadastrar os dados");
                }

                redirect('aluno/index');
            
            }
  
        }

    }

?>