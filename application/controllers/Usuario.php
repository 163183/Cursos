<?php

  class Usuario extends MY_Controller{

    public function __construct(){

        parent::__construct();

        if($this->session->userdata['logado']['admin']){
            $this->load->model('usuario_model');
        }else{
            redirect('login');
        }
    
    }

    public function index(){
        
        $dados['subtitulo']= "Usuário";
        $dados['titulo']= "Lista de Usuário";
        $dados['usuarios'] = $this->usuario_model->get();

        $this->template->load('template', 'usuario/lista-usuario', $dados);
    
    }

    public function cadastrar($id=null){
     
        $this->load->helper('form');
        $this->load->library('form_validation');

        $dados['subtitulo'] = "Usuário";
        $dados['titulo'] = "Cadastro de Usuários";
       
        $this->form_validation->set_rules('nome', 'Nome', 'required');
        $this->form_validation->set_rules('login', 'Login', 'required');
        $this->form_validation->set_rules('senha', 'senha', 'required');

        $dados['acao'] = "usuario/cadastrar/";
        $dados['registro'] = null; 

        if($id!==null){

            $dados['acao']    .= $id;
            $dados['registro'] = $this->usuario_model->get($id);
        
        }

        if($this->form_validation->run() === false){
            $this->template->load('template', 'usuario/formulario-usuario', $dados);
        }else{ 

            if(!$this->usuario_model->cadastrar($id)){
                die("Erro ao tentar cadastrar os dados");
            }

            redirect('usuario/index');
        
        }

    }

  }
  
 ?>
