<?php

    class Area extends CI_Controller{

        public function __construct(){

            parent::__construct();

            $this->load->helper('form');
            $this->load->model('area_model');

        }

        public function index(){

            $dados['subtitulo']= "Área";
            $dados['titulo']= "Lista de Área";
            $dados['areas'] = $this->area_model->get();

            $this->template->load('template', 'area/lista-area', $dados);
            
        }

        public function remover($id){
  
            if(!$this->area_model->remover($id)){
                die('Erro ao tentar remover');
            }
  
            redirect('area/index');
  
        }
  
        public function cadastrar($id=null){

            $this->load->helper('form');
            $this->load->library('form_validation');
            
            $dados['subtitulo'] = "Área";
            $dados['titulo'] = "Cadastro da Área";
  
            $this->form_validation->set_rules('nome', 'Nome', 'required');
    
            $dados['acao'] = "area/cadastrar/";
  
            $dados['registro'] = null; 
            if($id!==null){

                $dados['acao'] .= $id; 
                $dados['registro'] = $this->area_model->get($id);
            
            }

            $dados['areas'] = $this->area_model->get();
  
            if($this->form_validation->run()===false){
                $this->template->load('template', 'area/formulario-area', $dados);
            }else{
  
                if(!$this->area_model->cadastrar($id)){
                    die("Erro ao tentar cadastrar os dados");
                }

                redirect('area/index');
            
            }
  
        }

    }

 ?>