<?php

    require(APPPATH . 'libraries/fpdf181/' . 'fpdf.php');

    class MY_FPDF extends FPDF{

        public function Cell($w, $h=0, $txt='', $border=0, $ln=0, $align='', $fill=false, $link=''){
      
            $txt = utf8_decode($txt);
            parent::Cell($w, $h, $txt, $border, $ln, $align, $fill, $link);
      
        }

        function Header() {
            
            $this->Image(APPPATH.'../assets/img/logo-upf.png',10,6,30);
            
            $this->SetFont('Arial', 'B', 15);
            
            $this->Cell(40);
            
            $this->Cell(100, 10, 'Relatório', 0, 0, 'C');
   
            $this->Ln(20);

        }

        function Footer() {

            $this->SetY(-15);
            $this->SetFont('Arial','I',8);
            $this->Cell(0, 10, ' Pag. '.$this->PageNo().'/{nb}'.' - ' .date('d/m/Y'), 0, 0, 'C');

        }

    }

?>