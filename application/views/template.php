<!DOCTYPE html>

<html>

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Curso</title>

  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="<?= base_url('assets/bower_components/bootstrap/dist/css/bootstrap.min.css'); ?>">
  <link rel="stylesheet" href="<?= base_url('assets/bower_components/font-awesome/css/font-awesome.min.css'); ?>">
  <link rel="stylesheet" href="<?= base_url('assets/bower_components/Ionicons/css/ionicons.min.css'); ?>">
  <link rel="stylesheet" href="<?= base_url('assets/dist/css/AdminLTE.min.css'); ?>">
  <link rel="stylesheet" href="<?= base_url('assets/dist/css/skins/skin-blue.min.css'); ?>">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

</head>

<body class="hold-transition skin-blue sidebar-mini">

  <div class="wrapper">

    <header class="main-header">

      <a class="logo">

        <span class="logo-lg"><b>C</b>urso</span>
        <span class="logo-mini"><b>C</b></span>

      </a>

      <nav class="navbar navbar-static-top" role="navigation">

        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
          <span class="sr-only"></span>
        </a>

        <div class="navbar-custom-menu">

          <ul class="nav navbar-nav">

            <li class="dropdown user user-menu">

              <a href="#" class="dropdown-toggle" data-toggle="dropdown">

                <img src="<?= base_url($this->session->userdata['logado']['imagemUsuario']); ?>" 
                  class="user-image" alt="User Image">
                
                <span class="hidden-xs"><?= $this->session->userdata['logado']['nome']; ?>
                </span>

              </a>
              
              <ul class="dropdown-menu">
              
                <li class="user-header">

                  <img src="<?= base_url($this->session->userdata['logado']['imagemUsuario']); ?>" 
                    class="img-circle" alt="User Image">

                  <p>
                    
                    <?= $this->session->userdata['logado']['nome']; ?>
                    <small>Desde: 
                      <?= date("d/m/Y", strtotime($this->session->userdata['logado']['dataCadastro'])); ?>
                    </small>

                  </p>

                </li>
    
                <li class="user-footer">
    
                  <div class="pull-left">
                    <a href="<?= site_url('usuario/cadastrar/'.$this->session->userdata['logado']['id']); ?>" class="btn btn-default btn-flat">Editar</a>
                  </div>
    
                  <div class="pull-right">
                  
                    <a href="<?= site_url('login/logout');?>" 
                      class="btn btn-default btn-flat">Deslogar</a>
                  
                  </div>
    
                </li>
    
              </ul>

            </li>

            <li>
              <a href="<?= site_url('login/logout');?>" ><i class="fa fa-sign-out"></i></a>
            </li>

          </ul>

        </div>

      </nav>

    </header>
        
    <aside class="main-sidebar">

      <section class="sidebar">

        <ul class="sidebar-menu" data-widget="tree">
          
          <li class="header">Menu</li>          

          <li>
            
            <a href="<?= site_url('aluno'); ?>">
              <i class="fa fa-address-card"></i> <span>Aluno</span>
            </a>

          </li>

          <li>
            
            <a href="<?= site_url('area'); ?>">
              <i class="fa fa-link"></i> <span>Área</span>
            </a>

          </li>

          
          <li>
            
            <a href="<?= site_url('curso'); ?>">
              <i class="fa fa-address-book"></i> <span>Curso</span>
            </a>

          </li>

          <?php if($this->session->userdata['logado']['admin']){ ?>
            
            <li>
              
              <a href="<?= site_url('usuario'); ?>">
                <i class="fa fa-user-circle"></i> <span>Usuário</span>
              </a>

            </li>
          

            <li class="treeview">
              
              <a href="#"><i class="fa fa-file-pdf-o"></i> <span>Relatórios</span>
                
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>

              </a>
          
              <ul class="treeview-menu">
                
                <li><a href="<?= site_url('relatorio/formularioAlunoCurso'); ?>">Aluno por Curso</a></li>
              
                <li><a href="<?= site_url('relatorio/usuarioInativoPDF'); ?>" target="_blank" >Usuários Inativos</a></li>

              </ul>

            </li>
        
          <?php } ?>

        </ul>

      </section>

    </aside>

    <div class="content-wrapper">

      <section class="content-header">

        <h1>
        
          <small>
            <?php if(isset($subtitulo)) echo $subtitulo; ?>
          </small>
          
          <br/>

          <?php echo $titulo; ?>
        
        </h1>

      </section>

      <script src="<?= base_url('assets/bower_components/jquery/dist/jquery.min.js'); ?>"></script>

      <section class="content container-fluid">
          <?php echo $contents;  ?>
      </section>

    </div>

    <footer class="main-footer">
    
      <strong>
        Copyright &copy; 2019/2 ADS.
      </strong> 
    
      Todos os direitos reservados 
    
      <strong>
        <a href="#">:p</a> 
      </strong>.
    
    </footer>
  
  </div>

  <script src="<?= base_url('assets/bower_components/bootstrap/dist/js/bootstrap.min.js'); ?>"></script>
  <script src="<?= base_url('assets/dist/js/adminlte.min.js'); ?>"></script>

</body>

</html>