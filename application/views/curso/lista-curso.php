<div class="row">
    
    <div class="col-md-12">
    
        <div class="box">
    
          <div class="box-body">
    
            <a class="btn btn-primary" href="<?= site_url('curso/cadastrar'); ?>">
              <i class="fa fa-fw fa-plus"></i>
            </a>
    
            <table class="table table-hover table-striped">

              <thead>
                
                <th class="col-md-1">#</th>
                <th class="col-md-4">Nome</th>
                <th class="col-md-1">Data Cad.</th>
                <th class="col-md-1">Lim. Alunos</th>

                <th class="col-md-2">Ações</th>

              </thead>
            
              <tbody>
                <?php foreach($cursos as $curso):?>
                  
                  <tr>
                    
                    <td><?= $curso['id'];?></td>
                    <td><?= $curso['nome'];?></td>
                    <td><?= $curso['dataCadastro'];?></td>
                    <td><?= $curso['limiteAlunos'];?></td>

                    <td class="text-left">

                        <a class="btn btn-sm btn-info" href="<?= site_url('curso/cadastrar/'.$curso['id']); ?>">
                            <i class="fa fa-fw fa-edit"></i>
                        </a>

                        <a class="btn btn-sm btn-danger confirmaExclusao"  
                          data-id="<?= $curso['id'];?>"
                          data-nome="<?= $curso['nome'];?>">

                            <i class="fa fa-fw fa-trash"></i>
                       
                        </a>
                      
                    </td>

                  </tr>

                <?php endforeach; ?>

              </tbody>

            </table>

          </div>

        </div>

    </div>

</div>

<script type="text/javascript">
    
    $('.confirmaExclusao').on('click', function(e){
    
        e.preventDefault();
    
        var nome = $(this).data('nome');

        $('#nomeItem').text(nome);
        $('#modalConfirmacao').modal('show');
    
    });

    function remove(){
        var id = $('#modalConfirmacao').data('id');
        document.location.href = "<?= site_url('curso/remover/')?>" + id;
    }

</script>

 <!-- DataTables -->
<script src="<?= base_url('assets/bower_components/datatables.net/js/jquery.dataTables.min.js'); ?>"></script>
<script src="<?= base_url('assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js'); ?>"></script>

<script type="text/javascript">
  $(document).ready( function () {
      $('#tabelaDataTable').DataTable();
  } );
</script>

<!-- Modal de confirmação de exclusão -->
<div class="modal fade" id="modalConfirmacao">

  <div class="modal-dialog">

    <div class="modal-content">

      <div class="modal-header">

        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Atenção</h4>

      </div>

      <div class="modal-body">
        <p>Você tem certeza que deseja excluir o Curso: <span id="nomeItem"></span>?</p>
      </div>

      <div class="modal-footer">

        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-danger" onclick="remove();">Sim</button>
       
      </div>
     
    </div>
     
  </div>
   
</div>