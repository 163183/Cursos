<div class="row">

    <div class="col-md-12">

        <div class="box">

          <div class="box-body">

            <a class="btn btn-primary" href="<?= site_url('usuario/cadastrar'); ?>">
              <i class="fa fa-fw fa-plus"></i>
            </a>

            <table class="table table-hover table-striped">

              <thead>
              
                <th class="col-md-1">#</th>
                <th class="col-md-3">Nome</th>
                <th class="col-md-1">Login</th>
                <th class="col-md-1">E-mail</th>
                <th class="col-md-1">Telefone</th>
                <th class="col-md-1">Data Cad.</th>
                <th class="col-md-1">Ativo</th>
                <th class="col-md-1">Admin.</th>
                <th class="col-md-1">Ações</th>
              
              </thead>
              
              <tbody>

                <?php foreach($usuarios as $usuario):?>
              
                  <tr>

                    <td><?= $usuario['id'];?></td>
                    <td><?= $usuario['nome'];?></td>
                    <td><?= $usuario['login'];?></td>
                    <td><?= $usuario['email'];?></td>
                    <td><?= $usuario['telefone'];?></td>
                    <td><?= $usuario['dataCadastro'];?></td>
                    <td><?php echo ($usuario['ativo'])? 'Sim' : 'Não'; ?></td>
                    <td><?php echo ($usuario['admin'])? 'Sim' : 'Não'; ?></td>
                    
                    <td>
                    
                        <a class="btn btn-xs btn-info" 
                          href="<?= site_url('usuario/cadastrar/'.$usuario['id']); ?>">
                          
                            <i class="fa fa-fw fa-edit"></i>
                        
                        </a>
                                        
                    </td>

                  </tr>

                <?php endforeach; ?>
              
              </tbody>
            
            </table>
          
          </div>
        
        </div>
    
    </div>

</div>

 <!-- DataTables -->
<script src="<?= base_url('assets/bower_components/datatables.net/js/jquery.dataTables.min.js'); ?>"></script>
<script src="<?= base_url('assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js'); ?>"></script>

<script type="text/javascript">

  $(document).ready( function () {
    $('#tabelaDataTable').DataTable();
  });

</script>