<div class="row col-md-12">

    <div class="box">

        <div class="box-body">

          <?php if(validation_errors() != null){ ?>
          
            <div class="alert alert-danger alert-dismissible">
          
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-ban"></i> Erro!</h4>
                <?php echo validation_errors(); ?>
      
            </div>
          
          <?php } ?>

          <?php echo form_open($acao); ?>
            
            <a class="btn btn-danger confirmaSair" type="submit">Voltar</a>
            <button class="btn btn-success" type="submit">Enviar</button>

            <hr/>

            <div class="col-xs-12">
              
              <label for="nome">Nome</label>
              <input id="nome" class="form-control" type="text" name="nome" 
                value="<?= set_value('nome', $registro['nome']); ?>" 
                maxlength="60" required>
            
            </div>
            
            <div class="col-xs-6">
              
              <label for="nome">CPF</label>
              <input id="cpf" class="form-control" type="text" name="cpf" 
                value="<?= set_value('cpf', $registro['cpf']); ?>" 
                maxlength="11" required>
            
            </div>

            <div class="col-xs-6">
              
              <label for="nome">RG</label>
              <input id="rg" class="form-control" type="text" name="rg" 
                value="<?= set_value('rg', $registro['rg']); ?>" 
                maxlength="20" required>
            
            </div>

            <div class="col-xs-6">
              <label for="dataNascimento">Data Nascimento</label>
              <input id="dataNascimento" class="form-control" type="date" name="dataNascimento" 
                value="<?= set_value('dataNascimento', $registro['dataNascimento']); ?>">
            </div>

            <div class="col-xs-6">
              
              <label for="curso_id">Curso</label>
              
              <select class="form-control" name="curso_id" required>

                <option value="">Selecione um item da lista</option>
              
                <?php foreach ($cursos as $curso): ?>

                    <option value="<?= $curso['id']; ?>" <?php if(isset($registro) && $curso['id'] == $registro['curso_id']) echo "selected";?>>
                        <?= $curso['nome']; ?>
                    
                    </option>

                <?php endforeach; ?>
            
              </select>
            
            </div>

          </form>
        
        </div>
    
    </div>

</div>

<script type="text/javascript">

  $('.confirmaSair').on('click', function(e){ 
    $('#modalConfirmacao').modal('show');
  });

  function sair(){
    document.location.href = "<?= site_url('aluno') ?>";
  }

</script>

<div class="modal fade" id="modalConfirmacao">

  <div class="modal-dialog">

    <div class="modal-content">

      <div class="modal-header">

        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Confirmação!</h4>

      </div>
  
      <div class="modal-body">
        <p>Você tem certeza que deseja abandonar o cadastro de Aluno?</p>
      </div>

      <div class="modal-footer">

        <button type="button" class="btn btn-default" data-dismiss="modal">Não</button>
        <button type="button" class="btn btn-danger" onclick="sair();">Sim</button>

      </div>

    </div>

  </div>

 </div>