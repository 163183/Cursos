<div class="row col-md-12">

    <div class="box">

        <div class="box-body">

          <?php if(validation_errors() != null){ ?>
          
            <div class="alert alert-danger alert-dismissible">
          
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-ban"></i> Erro!</h4>
                <?php echo validation_errors(); ?>
      
            </div>
          
          <?php } ?>

          <?php echo form_open($acao); ?>
            
            <a class="btn btn-danger confirmaSair" type="submit">Voltar</a>
            <button class="btn btn-success" type="submit">Enviar</button>

            <hr/>

            <div class="col-xs-12">
              
              <label for="nome">Nome</label>
              <input id="nome" class="form-control" type="text" name="nome" 
                value="<?= set_value('nome', $registro['nome']); ?>" 
                maxlength="60" required>
            
            </div>
          
          </form>
        
        </div>
    
    </div>

</div>

<script type="text/javascript">

  $('.confirmaSair').on('click', function(e){ 
    $('#modalConfirmacao').modal('show');
  });

  function sair(){
    document.location.href = "<?= site_url('area') ?>";
  }

</script>

<div class="modal fade" id="modalConfirmacao">

  <div class="modal-dialog">

    <div class="modal-content">

      <div class="modal-header">

        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Confirmação!</h4>

      </div>
  
      <div class="modal-body">
        <p>Você tem certeza que deseja abandonar o cadastro de Área?</p>
      </div>

      <div class="modal-footer">

        <button type="button" class="btn btn-default" data-dismiss="modal">Não</button>
        <button type="button" class="btn btn-danger" onclick="sair();">Sim</button>

      </div>

    </div>

  </div>

 </div>