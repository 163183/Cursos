<?php

  class PDF extends MY_FPDF {   
  
    function FancyTable($header, $usuarios) {

      $this->SetFillColor(138,177,219);
      $this->SetTextColor(255);
      $this->SetDrawColor(153,153,153);
      $this->SetLineWidth(.3);
      
      $this->SetFont('','B');

      $w = array(10, 50, 40, 28, 30, 20, 20);

      for($i=0; $i < count($header); $i++){
        $this->Cell($w[$i], 7, $header[$i], 1, 0, 'C', true);
      }

      $this->Ln();
        
      $this->SetFillColor(224,235,255);
      $this->SetTextColor(0);
      $this->SetFont('');
        
      $fill = false;
      $cont=1;
        
      foreach($usuarios as $row) {
              
        $this->Cell($w[0], 4, $row['id'], 'LR', 0, 'R', $fill);
      
        $this->Cell($w[1], 4, $row['nome'], 'LR', 0, 'L', $fill);
      
        $this->Cell($w[2], 4, $row['login'], 'LR', 0, 'L', $fill);
      
        $this->Cell($w[3], 4, $row['email'], 'LR', 0, 'L', $fill);

        $this->Cell($w[4], 4, ($row['admin'])? 'Sim':'Não', 'LR', 0, 'L', $fill);
        
        $this->Cell($w[5], 4, $row['telefone'],'LR', 0, 'L', $fill);

        $this->Cell($w[6], 4, date('d/m/Y', strtotime($row['dataCadastro'])),'LR', 0, 'R', $fill);
            
        $this->Ln();
    
        $fill = !$fill;
        
      }
        
      $this->Cell(array_sum($w),0,'','T');
    
    }

  }

  $pdf = new PDF();

  $header = array('#', 'Nome', 'Login', 'E-mail', 'Administrador', 'Telefone', 'Data Cad.');

  $pdf->SetFont('Arial', '', 8);
  
  $pdf->AliasNbPages();
  
  $pdf->AddPage();
  
  $pdf->FancyTable($header, $usuarios);
  
  $pdf->Output();

?>
