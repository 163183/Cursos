<?php

  class PDF extends MY_FPDF {   
  
    function FancyTable($header, $alunos) {

      $this->SetFillColor(138,177,219);
      $this->SetTextColor(255);
      $this->SetDrawColor(153,153,153);
      $this->SetLineWidth(.3);
      
      $this->SetFont('','B');

      $w = array(10, 40, 20, 18, 20, 20, 30, 40);

      for($i=0; $i < count($header); $i++){
        $this->Cell($w[$i], 7, $header[$i], 1, 0, 'C', true);
      }

      $this->Ln();
        
      $this->SetFillColor(224,235,255);
      $this->SetTextColor(0);
      $this->SetFont('');
        
      $fill = false;
      $cont=1;
        
      foreach($alunos as $row) {
              
        $this->Cell($w[0], 4, $row['id'], 'LR', 0, 'R', $fill);
      
        $this->Cell($w[1], 4, $row['alunoNome'], 'LR', 0, 'L', $fill);
      
        $this->Cell($w[2], 4, $row['cpf'], 'LR', 0, 'L', $fill);
      
        $this->Cell($w[3], 4, $row['rg'], 'LR', 0, 'L', $fill);
      
        $this->Cell($w[4], 4, date('d/m/Y', strtotime($row['dataNascimento'])),'LR', 0, 'R', $fill);
      
        $this->Cell($w[5], 4, date('d/m/Y', strtotime($row['dataCadastro'])),'LR', 0, 'R', $fill);
      
        $this->Cell($w[6], 4, $row['usuarioNome'],'LR', 0, 'L', $fill);
      
        $this->Cell($w[7], 4, $row['cursoNome'], 'LR', 0, 'L', $fill);
      
        $this->Ln();
    
        $fill = !$fill;
        
      }
        
      $this->Cell(array_sum($w),0,'','T');
    
    }

  }

  $pdf = new PDF();

  $header = array('#', 'Aluno', 'CPF', 'RG', 'Data Nasc.', 'Data Cad.', 'Usuario', 'Curso');

  $pdf->SetFont('Arial', '', 8);
  
  $pdf->AliasNbPages();
  
  $pdf->AddPage();
  
  $pdf->FancyTable($header, $alunos);
  
  $pdf->Output();

?>
