-- drop o banco de dados caso ja exista
DROP DATABASE IF EXISTS curso;

-- criar o banco de dados
CREATE DATABASE curso;

-- utiliza o banco de dados
USE curso;

-- cria as tabelas
CREATE TABLE usuario(
    id integer auto_increment primary key,
    nome varchar(60) not null,
    login varchar(10) not null unique,
    senha varchar(100) not null,
    email varchar(100),
    admin boolean,
    ativo boolean,
    telefone varchar(20),
    dataCadastro date,
    imagemUsuario text
);

INSERT INTO usuario (nome, login, senha, email, admin, telefone, dataCadastro, ativo, imagemUsuario) 
VALUES('Administrador', 'admin', '21232f297a57a5a743894a0e4a801fc3', '', true, '', CURDATE(), true, './imagens/user-default.jpg');

CREATE TABLE area(
    id integer auto_increment primary key,
    nome varchar(60) not null unique
);

CREATE TABLE curso(
    id integer auto_increment primary key,
    nome varchar(60) not null,
    descricao text,
    dataCadastro date not null,
    limiteAlunos integer not null,
    area_id integer not null,
    usuario_id integer not null
);

ALTER TABLE curso ADD CONSTRAINT fk_curso_usuario_id FOREIGN KEY(usuario_id) REFERENCES usuario(id);
ALTER TABLE curso ADD CONSTRAINT fk_curso_area_id FOREIGN KEY(area_id) REFERENCES area(id);

CREATE INDEX ind_curso_usuario_id ON curso(usuario_id) USING BTREE;
CREATE INDEX ind_area_id ON curso(area_id) USING BTREE;


CREATE TABLE aluno(
    id integer auto_increment primary key,
    nome varchar(60) not null,
    cpf varchar(11) not null unique,
    rg varchar(20),
    dataNascimento date not null,
    dataCadastro date not null,
    usuario_id integer not null,
    curso_id integer not null
);

ALTER TABLE aluno ADD CONSTRAINT fk_aluno_usuario_id FOREIGN KEY(usuario_id) REFERENCES usuario(id);
ALTER TABLE aluno ADD CONSTRAINT fk_aluno_curso_id FOREIGN KEY(curso_id) REFERENCES curso(id);

CREATE INDEX ind_aluno_curso_id ON aluno(curso_id) USING BTREE;
CREATE INDEX ind_aluno_usuario_id ON aluno(usuario_id) USING BTREE;