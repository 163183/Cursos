<?php

    class Usuario_model extends CI_Model{
        
        private $tabelaNome;
        
        public function __construct(){
            $this->tabelaNome = 'usuario';
        }

        public function get($id=null){
        
            if($id==null){
        
                $query = $this->db->get($this->tabelaNome);
                return $query->result_array();
        
            }
        
            $query = $this->db->get_where($this->tabelaNome, array('id'=>$id));
            return $query->row_array(); 

        }

        public function cadastrar($id=null){
        
            $registro = $this->input->post();
            
            $registro['admin'] = ($registro['admin'])? 1:0;
            $registro['ativo'] = ($registro['ativo'])? 1:0;
            $registro['senha'] = md5($registro['senha']);
            $registro['imagemUsuario'] = './imagens/' . $registro['login'] . '.jpg';

            if($id == null){ 
            
                $registro['dataCadastro'] = date('Y-m-d');

                return $this->db->insert($this->tabelaNome, $registro);

            }
        
            return $this->db->where(array('id' => $id))->update($this->tabelaNome, $registro);
        
        }

        public function buscarUsuarioAtivo($dados){

            $where = array('login = ' => $dados['login'], 'senha = ' => $dados['senha'], 'ativo' => 1);
            $query = $this->db->get_where($this->tabelaNome, $where);

            return $query->row_array();

        }
    
  }

 ?>