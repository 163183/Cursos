<?php

    class Aluno_model extends CI_Model{

        private $tabelaNome;

        public function __construct(){
            $this->tabelaNome = 'aluno';
        }

        public function get($id=null){

            if($id==null){
        
                $query = $this->db->get($this->tabelaNome);
                return $query->result_array();
        
            }
        
            $query = $this->db->get_where($this->tabelaNome, array('id'=>$id));
            return $query->row_array(); 
        
        }

        public function remover($id){
            return $this->db->where(array('id'=>$id))->delete($this->tabelaNome);
        }

        public function cadastrar($id=null){
        
            $registro = $this->input->post();
        
            if($id==null){

                $registro['dataCadastro'] = date('Y-m-d');
                $registro['usuario_id'] = $this->session->userdata['logado']['id'];

                return $this->db->insert($this->tabelaNome, $registro);
            
            }
         
            return $this->db->where(array('id'=>$id))->update($this->tabelaNome, $registro);
        
        }

    }

?>