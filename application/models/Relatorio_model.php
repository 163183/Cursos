<?php
  class Relatorio_model extends CI_Model{

    public function __construct(){ }

    public function getAlunoByCurso($curso){

        $this->db->select('a.id, a.nome as alunoNome, a.cpf, a.rg, a.dataNascimento, a.dataCadastro, u.nome as usuarioNome, c.nome as cursoNome');    
        $this->db->from('aluno a');
        $this->db->join('curso c', 'c.id = a.curso_id');
        $this->db->join('usuario u', 'u.id = a.usuario_id');

        $where = array('c.id = ' => $curso);

        $this->db->where($where);
        $query = $this->db->get();
        
        return $query->result_array();

    }

    public function getUsuariosInativos(){

        $this->db->select('*');    
        $this->db->from('usuario u');
        $this->db->where('!u.ativo');

        $query = $this->db->get();
        
        return $query->result_array();

    }

  }
 ?>
